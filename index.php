<!DOCTYPE html>
<?php
session_start();
if(!empty($_SESSION['logged'])){
	header('Location: home');
	exit();
}
?>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Selamat Datang di Optimum-S</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
</head>
<body class="bg-light">
	<div class="container py-5">
		<div class="row justify-content-center">
			<div class="col-md-6">
				<div class="card">
					<div class="card-body">
						
						<h1 class="text-center">Optimum-S</h1>
						<div class="text-center text-secondary mb-2">Optimalisasi Penganggaran Terintegrasi Menuju Masyarakat Sejahtera </div>
						<form action="login.php" method="POST">
							<?php if(!empty($_SESSION['error'])):?>
							<div class="alert alert-warning">
								<?=$_SESSION['error'] ?>
							</div>
							<?php unset($_SESSION['error'])?>
							<?php endif?>
							<div class="form-group mb-2">
								<label>Username</label>
								<div>
									<input type="text" name="username" class="form-control" placeholder="Username" />
								</div>
							</div>
							<div class="form-group mb-2">
								<label>Password</label>
								<div>
									<input type="Password" name="password" class="form-control" placeholder="Kata Sandi" />
								</div>
							</div>
							<div class="d-grid">
								<button class="btn btn-primary">Masuk ke Aplikasi</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>